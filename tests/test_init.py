# tests/test_init.py

import logging
import pytest
from cheesefactory_email import CfEmail

logger = logging.getLogger(__name__)


def test_cfemail_init():
    email = CfEmail(host='3.3.3.3', header_tags={'key1': 'value1', 'tag2': 'value2'}, username='bob', password='pass')
    assert email.host == '3.3.3.3'
    assert email.port == 25
    assert email.username == 'bob'
    assert email.password == 'pass'
    assert email.use_tls is False
    assert email.header_tags == {'key1': 'value1', 'tag2': 'value2'}
    assert email.sender is None
    assert email.recipients is None
    assert email.subject is None
    assert email.body is None
    assert email.attachments is None


# noinspection PyTypeChecker
def test_cfemail_init_check():
    logger.debug('testing host')
    with pytest.raises(ValueError) as e:
        CfEmail(host=234, header_tags={'key1': 'value1', 'tag2': 'value2'}, username='bob', password='pass')
    logger.debug(str(e.value))
    assert 'host type is ' in str(e.value)

    logger.debug('testing use_tls')
    with pytest.raises(ValueError) as e:
        CfEmail(host='1.1.1.1', use_tls='yes', username='bob', password='pass')
    logger.debug(str(e.value))
    assert 'use_tls type is ' in str(e.value)

    logger.debug('testing header_tags')
    with pytest.raises(ValueError) as e:
        CfEmail(host='2.2.2.2', header_tags=['value1', 'value2'], username='bob', password='pass')
    logger.debug(str(e.value))
    assert 'header_tags type is ' in str(e.value)

    # logger.debug('testing port')
    # with pytest.raises(ValueError) as e:
    #     CfEmail(host='3.3.3.3', port=True, username='bob', password='pass')
    # logger.debug(str(e.value))
    # assert 'port type is ' in str(e.value)

    logger.debug('testing username')
    with pytest.raises(ValueError) as e:
        CfEmail(host='4.4.4.4', header_tags={'key1': 'value1', 'tag2': 'value2'}, username=234, password='pass')
    logger.debug(str(e.value))
    assert 'username type is ' in str(e.value)

    logger.debug('testing password')
    with pytest.raises(ValueError) as e:
        CfEmail(host='5.5.5.5', header_tags={'key1': 'value1', 'tag2': 'value2'}, username='bob', password=62)
    logger.debug(str(e.value))
    assert 'password type is ' in str(e.value)
